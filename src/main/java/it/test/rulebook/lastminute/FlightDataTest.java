package it.test.rulebook.lastminute;

import com.deliveredtechnologies.rulebook.Fact;
import com.deliveredtechnologies.rulebook.runner.RuleBookRunner;

public class FlightDataTest
{
  public static void main(String[] args)
  {
    RuleBookRunner ruleBookRunner = new RuleBookRunner("it.test.rulebook.lastminute");
    FlightData flightData = FlightData.FlightDataBuilder.aFlightData()
                                                        .withIdBusinessProfile("SKYSCANNERDE")
                                                        .withDepartureAirport("MXP")
                                                        .withArrivalAirport("JFK")
                                                        .withFareType("STD")
                                                        .withDriver("RUMBA")
                                                        .build();


    ruleBookRunner.withDeafultResult(false)
                  .given(
                    new Fact("idBusinessProfile", flightData.getIdBusinessProfile()),
                    new Fact("departureAirport", flightData.getDepartureAirport()),
                    new Fact("arrivalAirport", flightData.getArrivalAirport()),
                    new Fact("fareType", flightData.getFareType()),
                    new Fact("driver", flightData.getDriver())
                  )
                  .run();

    boolean voliGratis = (boolean) ruleBookRunner.getResult();

    if (voliGratis)
    {
      System.out.println("Voli Gratis");
    }
    else
    {
      System.out.println("Voli NON gratis!!! ");
    }
  }
}
