package it.test.rulebook;

import com.deliveredtechnologies.rulebook.RuleBook;
import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.StandardRule;

public class HelloWorldRuleBook extends RuleBook
{
  protected void defineRules()
  {
    addRule(StandardRule.create()
    .when(factsMap -> factsMap.containsKey("hello"))
    .then(factMap -> {
      System.out.println(factMap.getValue("hello"));
      return RuleState.NEXT;
    }));

    addRule(StandardRule.create()
    .when(factMap->factMap.containsKey("world"))
    .then(factMap->
    {
      System.out.println(factMap.getValue("world"));
      return RuleState.BREAK;
    }));
  }
}
