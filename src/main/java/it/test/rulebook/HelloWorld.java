package it.test.rulebook;

import com.deliveredtechnologies.rulebook.Fact;

public class HelloWorld
{

  public static void main(String[] args)
  {
//    new PrintHelloRule(new PrintWorldRule())
//      .print();
    new HelloWorldRuleBook()
      .given(new Fact("hello", "Hello"),
        new Fact("world", "World"))
      .run();
  }

  private static abstract class Rule
  {
    Rule successor;

    public Rule(final Rule successor)
    {
      this.successor = successor;
    }

    void print()
    {
      innerPrint();
      if (successor != null)
      {
        successor.print();
      }
    }


    abstract void innerPrint();
  }

  private static class PrintHelloRule extends Rule
  {
    PrintHelloRule(final Rule successor)
    {
       super(successor);
    }

    @Override
    void innerPrint()
    {
      System.out.println("Hello");
    }
  }

  private static class PrintWorldRule extends Rule
  {
    PrintWorldRule()
    {
      super(null);
    }

    @Override
    void innerPrint()
    {
      System.out.println("World");
    }

  }
}
