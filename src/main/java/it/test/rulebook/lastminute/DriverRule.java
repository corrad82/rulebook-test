package it.test.rulebook.lastminute;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Given;
import com.deliveredtechnologies.rulebook.annotation.Result;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

@Rule(order = 4)
public class DriverRule
{
  @Result
  private boolean result;

  @Given("driver")
  String idDriver;

  @When
  public boolean when()
  {
    return new RuleRepository().idDriver().equals(idDriver);
  }

  @Then
  public RuleState then()
  {
    result = true;
    return RuleState.BREAK;
  }

  private class RuleRepository
  {
    public String idDriver()
    {
      return "RUMBA";
    }
  }
}
