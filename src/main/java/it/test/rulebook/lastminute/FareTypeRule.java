package it.test.rulebook.lastminute;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Given;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

@Rule(order = 3)
public class FareTypeRule
{


  @Given("fareType")
  String fareType;

  @When
  public boolean when()
  {
    return !"STD".equals(fareType);
  }

  @Then
  public RuleState then()
  {
    return RuleState.BREAK;
  }

}
