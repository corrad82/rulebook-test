package it.test.rulebook.lastminute;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Given;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

@Rule(order = 1)
public class BusinessProfileRule
{

  @Given("idBusinessProfile")
  String idBusinessProfile;

  @When
  public boolean when()
  {
    return !idBusinessProfile.equals("SKYSCANNERDE");
  }

  @Then
  public RuleState then()
  {
    return RuleState.BREAK;
  }

}
