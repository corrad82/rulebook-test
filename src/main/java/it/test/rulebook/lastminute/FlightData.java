package it.test.rulebook.lastminute;

public class FlightData
{
  private String idBusinessProfile;
  private String fareType;
  private String departureAirport;
  private String arrivalAirport;
  private String driver;

  public String getIdBusinessProfile()
  {
    return idBusinessProfile;
  }

  public String getFareType()
  {
    return fareType;
  }

  public String getDepartureAirport()
  {
    return departureAirport;
  }

  public String getArrivalAirport()
  {
    return arrivalAirport;
  }

  public String getDriver()
  {
    return driver;
  }

  public static final class FlightDataBuilder
  {
    private String idBusinessProfile;
    private String fareType;
    private String departureAirport;
    private String arrivalAirport;
    private String driver;

    private FlightDataBuilder()
    {
    }

    public static FlightDataBuilder aFlightData()
    {
      return new FlightDataBuilder();
    }

    public FlightDataBuilder withIdBusinessProfile(String idBusinessProfile)
    {
      this.idBusinessProfile = idBusinessProfile;
      return this;
    }

    public FlightDataBuilder withFareType(String fareType)
    {
      this.fareType = fareType;
      return this;
    }

    public FlightDataBuilder withDepartureAirport(String departureAirport)
    {
      this.departureAirport = departureAirport;
      return this;
    }

    public FlightDataBuilder withArrivalAirport(String arrivalAirport)
    {
      this.arrivalAirport = arrivalAirport;
      return this;
    }

    public FlightDataBuilder withDriver(String driver)
    {
      this.driver = driver;
      return this;
    }

    public FlightData build()
    {
      FlightData flightData = new FlightData();
      flightData.arrivalAirport = this.arrivalAirport;
      flightData.departureAirport = this.departureAirport;
      flightData.idBusinessProfile = this.idBusinessProfile;
      flightData.fareType = this.fareType;
      flightData.driver = this.driver;
      return flightData;
    }
  }
}
