package it.test.rulebook.lastminute;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Given;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

@Rule(order = 2)
public class RouteRule
{
  @Given("departureAirport")
  String departureAirport;

  @Given("arrivalAirport")
  String arrivalAirport;

  @When
  public boolean when()
  {
    return !departureAirport.equals("MXP") || !arrivalAirport.equals("JFK");
  }

  @Then
  public RuleState then()
  {
    return RuleState.BREAK;
  }

}
